package kz.solva.task.mapper;

import kz.solva.task.model.dtos.LimitCreateDTO;
import kz.solva.task.model.entities.Limit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Маппер для преобразования объектов между классами {@link LimitCreateDTO} и {@link Limit}.
 */
@Mapper
public abstract class LimitMapper {

    /**
     * Преобразует объект {@link LimitCreateDTO} в объект {@link Limit}.
     *
     * @param limitCreateDTO объект {@link LimitCreateDTO}, который необходимо преобразовать
     * @return объект {@link Limit}, полученный в результате преобразования
     */
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "amount", source = "amount")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "setDate", expression = "java(java.time.LocalDateTime.now())")
    @Mapping(target = "lastLimit" , ignore = true)
    public abstract Limit toLimit(LimitCreateDTO limitCreateDTO);
}

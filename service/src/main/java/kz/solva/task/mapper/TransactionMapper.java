package kz.solva.task.mapper;

import kz.solva.task.model.dtos.TransactionDTO;
import kz.solva.task.model.dtos.TransactionLimitDTO;
import kz.solva.task.model.entities.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Маппер для преобразования объектов между классами {@link TransactionDTO}, {@link Transaction} и {@link TransactionLimitDTO}.
 */
@Mapper
public abstract class TransactionMapper {

    /**
     * Преобразует объект {@link TransactionDTO} в объект {@link Transaction}.
     *
     * @param transactionDTO объект {@link TransactionDTO}, который необходимо преобразовать
     * @return объект {@link Transaction}, полученный в результате преобразования
     */
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "amount", source = "amount")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "transactionDate", source = "transactionDate")
    @Mapping(target = "limitExceeded", ignore = true, defaultValue = "false")
    @Mapping(target = "limit", ignore = true)
    public abstract Transaction mapToTransaction(TransactionDTO transactionDTO);

    /**
     * Преобразует объект {@link Transaction} в объект {@link TransactionLimitDTO}.
     *
     * @param transaction объект {@link Transaction}, который необходимо преобразовать
     * @return объект {@link TransactionLimitDTO}, полученный в результате преобразования
     */
    @Mapping(target = "id", source = "id")
    @Mapping(target = "amount", source = "amount")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "transactionDate", source = "transactionDate")
    @Mapping(target = "limitDTO", source = "limit")
    public abstract TransactionLimitDTO mapToTransactionLimit(Transaction transaction);

}

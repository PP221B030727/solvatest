package kz.solva.task.mapper;

import kz.solva.task.model.dtos.CurrencyDto;
import kz.solva.task.model.dtos.ExchangeRateDTO;
import kz.solva.task.model.entities.Currency;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Маппер для преобразования объектов между классами {@link ExchangeRateDTO.RealtimeCurrencyExchangeRate}, {@link Currency} и {@link CurrencyDto}.
 */
@Mapper
public abstract class CurrencyMapper {

    /**
     * Преобразует объект {@link ExchangeRateDTO.RealtimeCurrencyExchangeRate} в объект {@link Currency}.
     *
     * @param realtimeCurrencyExchangeRate объект {@link ExchangeRateDTO.RealtimeCurrencyExchangeRate}, который необходимо преобразовать
     * @return объект {@link Currency}, полученный в результате преобразования
     */
    @Mapping(target = "currency", source = "toCurrencyCode")
    @Mapping(target = "baseCurrency", source = "fromCurrencyCode")
    @Mapping(target = "rate", source = "exchangeRate")
    @Mapping(target = "date", source = "lastRefreshed")
    public abstract Currency exchangeRateToCurrency(ExchangeRateDTO.RealtimeCurrencyExchangeRate realtimeCurrencyExchangeRate);

    /**
     * Преобразует строку даты и времени в объект {@link LocalDateTime}.
     *
     * @param dateTimeString строка даты и времени в формате "yyyy-MM-dd HH:mm:ss"
     * @return объект {@link LocalDateTime}, представляющий дату и время из переданной строки
     */
    public LocalDateTime parseDate(String dateTimeString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeString, formatter);
        return dateTime;
    }

    /**
     * Преобразует объект {@link Currency} в объект {@link CurrencyDto}.
     *
     * @param currency объект {@link Currency}, который необходимо преобразовать
     * @return объект {@link CurrencyDto}, полученный в результате преобразования
     */
    @Mapping(target = "baseCurrency", source = "baseCurrency")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "rate", source = "rate")
    @Mapping(target = "date", source = "date")
    public abstract CurrencyDto toCurrencyDto(Currency currency);

}

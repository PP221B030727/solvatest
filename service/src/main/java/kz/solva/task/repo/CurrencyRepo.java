package kz.solva.task.repo;

import jakarta.annotation.Nonnull;
import kz.solva.task.model.entities.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CurrencyRepo extends JpaRepository<Currency, Long> {
    /**
     * Находит список валют по базовой и целевой валютам, отсортированный по дате в убывающем порядке.
     *
     * @param from базовая валюта
     * @param to целевая валюта
     * @return список валют
     */
    @Query(name = "Currency.findByBaseCurrencyAndCurrencyOrderByDateDesc", nativeQuery = true)
    List<Currency> findByBaseCurrencyAndCurrencyOrderByDateDesc(@Nonnull @Param("from") String from, @Nonnull @Param("to") String to);

    /**
     * Находит список валют по базовой и целевой валютам на указанную дату.
     *
     * @param from базовая валюта
     * @param to целевая валюта
     * @param date дата
     * @return список валют
     */
    @Query(name = "Currency.findByBaseCurrencyAndCurrencyAndDate", nativeQuery = true)
    List<Currency> findByBaseCurrencyAndCurrencyAndDate(@Nonnull @Param("from") String from, @Nonnull @Param("to") String to, @Nonnull @Param("date") LocalDateTime date);


}

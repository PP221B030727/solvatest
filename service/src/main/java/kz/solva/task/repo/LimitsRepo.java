package kz.solva.task.repo;

import jakarta.annotation.Nonnull;
import kz.solva.task.model.entities.Limit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LimitsRepo extends JpaRepository<Limit, Long> {
    /**
     * Получение лимита по категории
     *
     * @param category название категории
     * @return найденную запись.
     */
    @Query(name = "Limit.findByCategory", nativeQuery = true)
    Limit findByCategory(@Nonnull @Param("cat") String category);
}

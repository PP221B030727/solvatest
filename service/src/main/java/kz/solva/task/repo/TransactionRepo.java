package kz.solva.task.repo;

import jakarta.annotation.Nonnull;
import kz.solva.task.model.entities.Currency;
import kz.solva.task.model.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepo extends JpaRepository<Transaction, Long> {
    /**
     * Находит список транзакций с указанием превышения лимита.
     *
     * @param isExceeded флаг превышения лимита
     * @return список транзакций
     */
    @Query(name = "Transaction.findByLimitExceeded", nativeQuery = true)
    List<Transaction> findByLimitExceeded(@Nonnull @Param("isExceeded") Boolean isExceeded);
}

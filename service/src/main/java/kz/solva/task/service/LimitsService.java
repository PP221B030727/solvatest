package kz.solva.task.service;

import kz.solva.task.model.dtos.LimitCreateDTO;

import kz.solva.task.model.entities.Limit;

import java.math.BigDecimal;
import java.util.List;

public interface LimitsService {
    Limit getDefaulLimit();
    Limit getByCategory(String category);
    Boolean checkLimitExceed(BigDecimal amount , BigDecimal limitAmount);

    List<Limit> getAll();

    void createLimit(LimitCreateDTO limitDTO);

    Boolean checkLimitExists(String category);
}

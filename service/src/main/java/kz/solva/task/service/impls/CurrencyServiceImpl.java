package kz.solva.task.service.impls;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.solva.task.config.CurrencyConfig;
import kz.solva.task.mapper.CurrencyMapper;
import kz.solva.task.model.dtos.CurrencyDto;
import kz.solva.task.model.dtos.ExchangeRateDTO;
import kz.solva.task.model.entities.Currency;
import kz.solva.task.model.entities.Limit;
import kz.solva.task.repo.CurrencyRepo;
import kz.solva.task.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

/**
 * Сервис для работы с курсами валют.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepo currencyRepo;
    private final CurrencyConfig currencyConfig;
    private final CurrencyMapper currencyMapper;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Получает данные о курсе валютной пары от внешнего API.
     *
     * @param from валюта, из которой происходит конвертация
     * @param to валюта, в которую происходит конвертация
     * @return данные о курсе валютной пары
     * @throws IOException если произошла ошибка ввода-вывода
     * @throws InterruptedException если поток был прерван
     */
    @Override
    public ExchangeRateDTO getCurrencyPair(String from, String to) throws IOException, InterruptedException {
        log.info("Sending request to API for getting data with currency");
        val urlString = String.format(currencyConfig.getCurrencyUrl(), from, to, currencyConfig.getCurrencyApiKey());
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(urlString))
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        try {
            ExchangeRateDTO exchangeRateDTO = objectMapper.readValue(response.body(), ExchangeRateDTO.class);
            mapAndSaveCurrency(exchangeRateDTO.getRealtimeCurrencyExchangeRate());
            return exchangeRateDTO;
        } catch (Exception e) {
            log.error("Some error occurred when processing data", e);
            throw new InterruptedException();
        }
    }

    /**
     * Преобразует и сохраняет данные о курсе валюты.
     *
     * @param realtimeCurrencyExchangeRate данные о курсе валюты
     */
    @Override
    public void mapAndSaveCurrency(ExchangeRateDTO.RealtimeCurrencyExchangeRate realtimeCurrencyExchangeRate) {
        val currency = currencyMapper.exchangeRateToCurrency(realtimeCurrencyExchangeRate);
        if (checkDailyCurrencyExists(currency)) {
            log.info("Currency saved");
            currencyRepo.save(currency);
        }
    }

    /**
     * Получает последний курс валютной пары.
     *
     * @param from валюта, из которой происходит конвертация
     * @param to валюта, в которую происходит конвертация
     * @return данные о последнем курсе валютной пары
     */
    @Override
    public CurrencyDto getLastCurrency(String from, String to) {
        val lastCurrencies = currencyRepo.findByBaseCurrencyAndCurrencyOrderByDateDesc(from, to);
        if (!lastCurrencies.isEmpty()) {
            return lastCurrencies.stream().findFirst().map(currencyMapper::toCurrencyDto).get();
        }
        return null;
    }

    /**
     * Проверяет, существует ли ежедневный курс валюты.
     *
     * @param currency данные о курсе валюты
     * @return true, если курс валюты на текущую дату не существует, иначе false
     */
    @Override
    public Boolean checkDailyCurrencyExists(Currency currency) {
        val sameCurrencies = currencyRepo.findByBaseCurrencyAndCurrencyAndDate(currency.getBaseCurrency(), currency.getCurrency(), currency.getDate());
        return sameCurrencies.isEmpty();
    }
}

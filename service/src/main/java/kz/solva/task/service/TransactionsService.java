package kz.solva.task.service;

import kz.solva.task.model.dtos.TransactionDTO;
import kz.solva.task.model.dtos.TransactionLimitDTO;

import java.util.List;

public interface TransactionsService {
    void createTransaction(TransactionDTO transactionDTO);
    List<TransactionLimitDTO> getExceedLimit();
}

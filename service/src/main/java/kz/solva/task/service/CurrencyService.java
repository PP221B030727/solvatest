package kz.solva.task.service;

import kz.solva.task.model.dtos.CurrencyDto;
import kz.solva.task.model.dtos.ExchangeRateDTO;
import kz.solva.task.model.entities.Currency;

import java.io.IOException;

public interface CurrencyService {
    ExchangeRateDTO getCurrencyPair(String from, String to) throws IOException, InterruptedException;
    void mapAndSaveCurrency(ExchangeRateDTO.RealtimeCurrencyExchangeRate realtimeCurrencyExchangeRate);
    CurrencyDto getLastCurrency(String from, String to);
    Boolean checkDailyCurrencyExists(Currency currency);
}

package kz.solva.task.service.impls;

import kz.solva.task.exception.BadLimitException;
import kz.solva.task.exception.LimitAlreadyExists;
import kz.solva.task.exception.LimitNotFoundException;
import kz.solva.task.mapper.LimitMapper;
import kz.solva.task.model.dtos.LimitCreateDTO;
import kz.solva.task.model.dtos.LimitDTO;
import kz.solva.task.model.entities.Limit;
import kz.solva.task.repo.LimitsRepo;
import kz.solva.task.service.LimitsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Objects.isNull;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import java.math.BigDecimal;
import java.util.List;
import static java.util.Objects.isNull;

/**
 * Сервис для работы с лимитами.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LimitsServiceImpl implements LimitsService {

    private final LimitsRepo limitsRepo;
    private final LimitMapper limitMapper;

    /**
     * Получает лимит по умолчанию.
     *
     * @return лимит по умолчанию
     * @throws LimitNotFoundException если лимит не найден
     */
    @Override
    public Limit getDefaulLimit() {
        try {
            val limit = limitsRepo.findByCategory("DEFAULT");
            return limit;
        } catch (Exception e) {
            throw new LimitNotFoundException();
        }
    }

    /**
     * Получает лимит по указанной категории.
     *
     * @param category категория лимита
     * @return лимит
     * @throws LimitNotFoundException если лимит не найден
     */
    @Override
    public Limit getByCategory(String category) {
        try {
            val limit = limitsRepo.findByCategory(category);
            return limit;
        } catch (Exception e) {
            log.error("Limit Not Found");
            throw new LimitNotFoundException();
        }
    }

    /**
     * Проверяет, превышен ли лимит.
     *
     * @param amount      сумма транзакции
     * @param limitAmount сумма лимита
     * @return true, если лимит превышен, иначе false
     */
    @Override
    public Boolean checkLimitExceed(BigDecimal amount, BigDecimal limitAmount) {
        return amount.compareTo(limitAmount) > 0;
    }

    /**
     * Получает все лимиты.
     *
     * @return список лимитов
     */
    @Override
    public List<Limit> getAll() {
        return limitsRepo.findAll();
    }

    /**
     * Создает новый лимит.
     *
     * @param limitDTO информация о новом лимите
     * @throws BadLimitException если возникла проблема с созданием лимита
     * @throws LimitAlreadyExists если лимит уже существует
     */
    @Override
    public void createLimit(LimitCreateDTO limitDTO) {
        if (!checkLimitExists(limitDTO.getCategory())) {
            val limit = limitMapper.toLimit(limitDTO);
            try {
                limit.setLastLimit(true);
                limitsRepo.save(limit);
            } catch (Exception e) {
                throw new BadLimitException();
            }
        } else {
            throw new LimitAlreadyExists();
        }
    }

    /**
     * Проверяет, существует ли лимит с указанной категорией.
     *
     * @param category категория лимита
     * @return true, если лимит существует, иначе false
     */
    @Override
    public Boolean checkLimitExists(String category) {
        try {
            return !isNull(limitsRepo.findByCategory(category));
        } catch (Exception e) {
            log.error("limit not found");
            return false;
        }
    }
}

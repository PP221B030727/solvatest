package kz.solva.task.service.impls;

import kz.solva.task.exception.BadTransactionException;
import kz.solva.task.exception.TransactionNotFoundException;
import kz.solva.task.mapper.TransactionMapper;
import kz.solva.task.model.dtos.TransactionDTO;
import kz.solva.task.model.dtos.TransactionLimitDTO;
import kz.solva.task.repo.TransactionRepo;
import kz.solva.task.service.LimitsService;
import kz.solva.task.service.TransactionsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы с транзакциями.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionsServiceImpl implements TransactionsService {

    private final TransactionRepo transactionRepo;
    private final TransactionMapper transactionMapper;
    private final LimitsService limitsService;

    /**
     * Создает транзакцию.
     *
     * @param transactionDTO информация о транзакции
     */
    @Override
    @Transactional
    public void createTransaction(TransactionDTO transactionDTO) {
        log.info("Start receiving transaction: {}", transactionDTO.getTransactionDate());
        val transaction = Optional.of(transactionMapper.mapToTransaction(transactionDTO));
        try {
            transaction.ifPresent(t -> {
                t.setLimit(limitsService.getByCategory(transactionDTO.getCategory()));
                t.setLimitExceeded(limitsService.checkLimitExceed(t.getAmount(), t.getLimit().getAmount()));
                transactionRepo.save(t);
            });
        } catch (Exception e) {
            throw new BadTransactionException();
        }
    }

    /**
     * Получает транзакции, превышающие лимит.
     *
     * @return список транзакций, превышающих лимит
     * @throws TransactionNotFoundException если транзакции не найдены
     */
    @Override
    public List<TransactionLimitDTO> getExceedLimit() {
        val result = transactionRepo.findByLimitExceeded(true);
        if (!result.isEmpty()) {
            return result.stream().map(transactionMapper::mapToTransactionLimit).toList();
        }
        throw new TransactionNotFoundException();
    }
}
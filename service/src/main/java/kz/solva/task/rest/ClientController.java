package kz.solva.task.rest;

import kz.solva.task.exception.CurrencyNotFoundException;
import kz.solva.task.model.dtos.LimitCreateDTO;
import kz.solva.task.service.CurrencyService;
import kz.solva.task.service.LimitsService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static java.util.Objects.isNull;


/**
 * Контроллер для взаимодействия с клиентом.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientController {

    private final CurrencyService currencyService;
    private final LimitsService limitsService;

    /**
     * Получает информацию о курсе валютной пары.
     *
     * @param from валюта, из которой происходит конвертация
     * @param to валюта, в которую происходит конвертация
     * @return ResponseEntity с данными о курсе валютной пары
     * @throws CurrencyNotFoundException если информация о курсе валютной пары не найдена
     */
    @GetMapping("/exchange-rates/currency-pairs")
    ResponseEntity<?> getCurrencyPair(@RequestParam("from") String from, @RequestParam("to") String to) {
        try {
            val res = currencyService.getCurrencyPair(from, to);
            return ResponseEntity.ok(res);
        } catch (IOException | InterruptedException e) {
            val res = currencyService.getLastCurrency(from, to);
            if (isNull(res)) {
                throw new CurrencyNotFoundException();
            }
            return ResponseEntity.ok(res);
        }
    }

    /**
     * Получает все лимиты.
     *
     * @return ResponseEntity с данными о всех лимитах
     */
    @GetMapping("/limit/all")
    ResponseEntity<?> getAllLimit() {
        return ResponseEntity.ok(limitsService.getAll());
    }

    /**
     * Создает новый лимит.
     *
     * @param limitDTO информация о новом лимите
     * @return ResponseEntity с сообщением о создании лимита
     */
    @PostMapping("/limit/new")
    ResponseEntity<?> createNewLimit(@RequestBody LimitCreateDTO limitDTO) {
        limitsService.createLimit(limitDTO);
        return ResponseEntity.ok("Limit created");
    }
}

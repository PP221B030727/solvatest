package kz.solva.task.rest;

import kz.solva.task.exception.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;



/**
 * Глобальный обработчик исключений для контроллеров.
 */
@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice(assignableTypes = {
        ClientController.class,
        TransactionController.class
})
public class BaseExceptionHandler {
    @ExceptionHandler(BadTransactionException.class)
    public ResponseEntity<String> handleCustomBadTransactionException(BadTransactionException ex){
        return new ResponseEntity<>("Exception : " + ex.getMessage(), ex.getStatus());
    }
    @ExceptionHandler(CurrencyNotFoundException.class)
    public ResponseEntity<String> handleCustomCurrencyNotFoundExceptiion(CurrencyNotFoundException ex){
        return new ResponseEntity<>("Exception : " + ex.getMessage(), ex.getStatus());
    }
    @ExceptionHandler(TransactionNotFoundException.class)
    public ResponseEntity<String> handleCustomTransactionNotFoundException(TransactionNotFoundException ex){
        return new ResponseEntity<>("Exception : " + ex.getMessage(), ex.getStatus());
    }
    @ExceptionHandler(BadLimitException.class)
    public ResponseEntity<String> handleCustomTransactionNotFoundException(BadLimitException ex){
        return new ResponseEntity<>("Exception : " + ex.getMessage(), ex.getStatus());
    }

    @ExceptionHandler(LimitAlreadyExists.class)
    public ResponseEntity<String> handleCustomEmployeeAlreadyExists(LimitAlreadyExists ex){
        return new ResponseEntity<>("Exception : " + ex.getMessage(), ex.getStatus());
    }
}

package kz.solva.task.rest;

import kz.solva.task.model.dtos.TransactionDTO;
import kz.solva.task.service.TransactionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Контроллер для работы с транзакциями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionsService transactionsService;

    /**
     * Получает и сохраняет транзакцию.
     *
     * @param transactionDTO информация о транзакции
     * @return ResponseEntity с сообщением о сохранении транзакции
     */
    @PostMapping
    ResponseEntity<?> getAndSaveTransaction(@RequestBody TransactionDTO transactionDTO) {
        transactionsService.createTransaction(transactionDTO);
        return ResponseEntity.ok("Transaction saved");
    }

    /**
     * Получает транзакции, превышающие лимит.
     *
     * @return ResponseEntity с данными о транзакциях, превышающих лимит
     */
    @GetMapping("/limit-exceeded")
    ResponseEntity<?> getLimitExceeded() {
        return ResponseEntity.ok(transactionsService.getExceedLimit());
    }

}

package kz.solva.task.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
@Getter
@Configuration
public class CurrencyConfig {
    @Value("${currency.url}")
    private String currencyUrl;

    @Value("${currency.apikey}")
    private String currencyApiKey;
}

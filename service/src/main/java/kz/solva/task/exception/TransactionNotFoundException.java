package kz.solva.task.exception;

import org.springframework.http.HttpStatus;


/**
 * Исключение, указывающее на то, что транзакция не найдена.
 * Это подкласс {@link AppException}.
 */
public class TransactionNotFoundException extends AppException {

    /**
     * Возвращает статус HTTP, соответствующий данному исключению (404 Not Found).
     *
     * @return статус HTTP
     */
    @Override
    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}

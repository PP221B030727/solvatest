package kz.solva.task.exception;

import org.springframework.http.HttpStatus;


/**
 * Исключение, которое указывает на то, что лимит уже существует.
 */
public class LimitAlreadyExists extends AppException {

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}

package kz.solva.task.exception;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Общее исключение приложения.
 * Расширяет класс {@link RuntimeException}.
 */
@NoArgsConstructor
public class AppException extends RuntimeException {

    /**
     * Конструктор с исходной причиной.
     *
     * @param cause исходная причина
     */
    public AppException(Throwable cause) {
        super(cause);
    }

    /**
     * Конструктор с сообщением и исходной причиной.
     *
     * @param message сообщение об ошибке
     * @param cause   исходная причина
     */
    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Конструктор с сообщением об ошибке.
     *
     * @param message сообщение об ошибке
     */
    public AppException(String message) {
        super(message);
    }

    /**
     * Возвращает статус HTTP по умолчанию для данного исключения (500 Internal Server Error).
     *
     * @return статус HTTP
     */
    public HttpStatus getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}

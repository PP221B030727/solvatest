package kz.solva.task.exception;

import org.springframework.http.HttpStatus;

/**
 * Исключение, указывающее на неверный лимит.
 * Это подкласс {@link AppException}.
 */
public class BadLimitException extends AppException {

    /**
     * Возвращает статус HTTP, соответствующий данному исключению (400 Bad Request).
     *
     * @return статус HTTP
     */
    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

}

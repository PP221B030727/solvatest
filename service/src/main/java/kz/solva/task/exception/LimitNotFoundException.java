package kz.solva.task.exception;

import org.springframework.http.HttpStatus;

/**
 * Исключение, указывающее на то, что лимит не найден.
 * Это подкласс {@link AppException}.
 */
public class LimitNotFoundException extends AppException {

    /**
     * Возвращает статус HTTP, соответствующий данному исключению (400 Bad Request).
     *
     * @return статус HTTP
     */
    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

}


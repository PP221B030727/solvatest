package kz.solva.task.exception;

import org.springframework.http.HttpStatus;




/**
 * Исключение, указывающее на некорректную транзакцию.
 * Это подкласс {@link AppException}.
 */
public class BadTransactionException extends AppException {

    /**
     * Возвращает статус HTTP, соответствующий данному исключению (400 Bad Request).
     *
     * @return статус HTTP
     */
    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

}

package kz.solva.task.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "limits")
public class Limit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, precision = 10, scale = 2)
    private BigDecimal amount;

    @Column(name = "category", nullable = false)
    private String category;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "set_date", nullable = false)
    private LocalDateTime setDate;

    @Column(name = "last_limit", nullable = false)
    private Boolean lastLimit;
}

package kz.solva.task.model.dtos;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


/**
 * DTO для представления информации о курсе обмена.
 */
@Data
public class ExchangeRateDTO {

    /**
     * Реальный курс обмена валют.
     */
    @JsonProperty("Realtime Currency Exchange Rate")
    private RealtimeCurrencyExchangeRate realtimeCurrencyExchangeRate;

    /**
     * Внутренний класс для представления информации о реальном курсе обмена валют.
     */
    @Data
    public static class RealtimeCurrencyExchangeRate {

        /**
         * Код исходной валюты.
         */
        @JsonProperty("1. From_Currency Code")
        private String fromCurrencyCode;

        /**
         * Название исходной валюты.
         */
        @JsonProperty("2. From_Currency Name")
        private String fromCurrencyName;

        /**
         * Код целевой валюты.
         */
        @JsonProperty("3. To_Currency Code")
        private String toCurrencyCode;

        /**
         * Название целевой валюты.
         */
        @JsonProperty("4. To_Currency Name")
        private String toCurrencyName;

        /**
         * Обменный курс.
         */
        @JsonProperty("5. Exchange Rate")
        private String exchangeRate;

        /**
         * Время последнего обновления.
         */
        @JsonProperty("6. Last Refreshed")
        private String lastRefreshed;

        /**
         * Часовой пояс.
         */
        @JsonProperty("7. Time Zone")
        private String timeZone;

        /**
         * Цена покупки.
         */
        @JsonProperty("8. Bid Price")
        private String bidPrice;

        /**
         * Цена продажи.
         */
        @JsonProperty("9. Ask Price")
        private String askPrice;
    }
}

package kz.solva.task.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;



/**
 * DTO для представления информации о лимите.
 */
@Data
public class LimitDTO {

    /**
     * Дата установки лимита.
     */
    @JsonProperty("set_date")
    private LocalDateTime setDate;

    /**
     * Сумма лимита.
     */
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * Валюта лимита.
     */
    @JsonProperty("currency")
    private String currency;

}

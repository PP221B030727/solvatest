package kz.solva.task.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;


/**
 * DTO для создания лимита.
 */
@Data
public class LimitCreateDTO {

    /**
     * Категория лимита.
     */
    @JsonProperty("category")
    private String category;

    /**
     * Сумма лимита.
     */
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * Валюта лимита.
     */
    @JsonProperty("currency")
    private String currency;
}

package kz.solva.task.model.dtos;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/**
 * DTO для представления информации о валюте.
 */
@Data
public class CurrencyDto {

    /**
     * Базовая валюта.
     */
    private String baseCurrency;

    /**
     * Валюта.
     */
    private String currency;

    /**
     * Курс обмена.
     */
    private BigDecimal rate;

    /**
     * Дата.
     */
    private Date date;
}

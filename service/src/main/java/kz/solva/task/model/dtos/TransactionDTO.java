package kz.solva.task.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * DTO для представления информации о транзакции.
 */
@Data
public class TransactionDTO {

    /**
     * Сумма транзакции.
     */
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * Валюта транзакции.
     */
    @JsonProperty("currency")
    private String currency;

    /**
     * Категория транзакции.
     */
    @JsonProperty("category")
    private String category;

    /**
     * Дата и время транзакции.
     */
    @JsonProperty("transaction_date")
    private LocalDateTime transactionDate;
}

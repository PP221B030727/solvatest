package kz.solva.task.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * DTO для представления информации о транзакции с учетом лимита.
 */
@Data
public class TransactionLimitDTO {

    /**
     * Идентификатор транзакции.
     */
    @JsonProperty("id")
    private Long id;

    /**
     * Сумма транзакции.
     */
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * Валюта транзакции.
     */
    @JsonProperty("currency")
    private String currency;

    /**
     * Категория транзакции.
     */
    @JsonProperty("category")
    private String category;

    /**
     * Дата транзакции.
     */
    @JsonProperty("transaction_date")
    private LocalDate transactionDate;

    /**
     * Информация о лимите, связанном с транзакцией.
     */
    @JsonProperty("limit")
    private LimitDTO limitDTO;
}